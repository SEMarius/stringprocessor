package com.mse.processor.services;

import com.mse.processor.domain.WordCount;
import com.mse.processor.repository.InputTextRepository;
import com.mse.processor.repository.WordCountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProcessorService {
    @Autowired
    private WordCountRepository wordCountRepository;
    @Autowired
    private InputTextRepository inputTextRepository;

    private Map<String, Integer> map = new HashMap<>();

    public void process() {
        //read from file
        try (Stream inputStream = Files.lines(Paths.get(new ClassPathResource("test.txt").getFile().getPath()))) {
            inputStream.filter(item -> !item.toString().isEmpty()).forEach(line -> this.processLine(line.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //read from DB
        this.inputTextRepository.findAll().stream().filter(line -> !line.toString().isEmpty()).forEach(text -> this.processLine(text.getText()));

        //save the process to DB and prints to console
        this.saveProcess();
    }

    /** Porcess line by line, splitting the words by space, replacing any non-alphabetic characters
     *  then storing the words in a map with the word as the key and the count as the value
     * */
    public void processLine(String line) {
        List<String> words = Arrays.asList(line.split(" "));

        words.forEach(word -> {
            word = word.replaceAll("[^A-Za-z]+", "").toLowerCase();

            if(map.keySet().contains(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        });
    }

    /**
     * Function to save the process.
     * It deletes everithing stored in the responses table
     * Sorts the list of words first by count, then in alphabetical oreder
     * Prints to console and saves the response to DB.
     */
    public void saveProcess() {
        wordCountRepository.deleteAll();

        List<WordCount>  list = map.entrySet().stream().map(WordCount::new).collect(Collectors.toList());
        list.sort(Comparator.comparing(WordCount::getCount).reversed().thenComparing(WordCount::getWord));
        list.stream().forEach(word -> {
            System.out.println(word.getWord() + " " + word.getCount());
            this.wordCountRepository.save(word);
        });
    }
}
