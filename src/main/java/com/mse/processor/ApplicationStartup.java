package com.mse.processor;

import com.mse.processor.services.ProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    private ProcessorService processorService;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        this.processorService.process();
        return;
    }
}
