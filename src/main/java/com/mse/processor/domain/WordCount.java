package com.mse.processor.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;

@Entity
@Table(name = "word_count")
public class WordCount {
    @Id
    private String word;
    private int count;

    public WordCount() {

    }

    public WordCount(Map.Entry<String, Integer> entry) {
        this.word = entry.getKey();
        this.count = entry.getValue();
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
