package com.mse.processor.repository;

import com.mse.processor.domain.InputText;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InputTextRepository extends JpaRepository<InputText, String> {
}
